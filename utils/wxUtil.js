export default class WxUtil {
	static wxPay(param) {
		return new Promise((resolve, reject) => {
			wx.requestPayment({
				...param,
				complete: res => {
				  resolve(res)
				}
			})
		})
	}
}

// wx.requestPayment(
// {
// 'timeStamp': '',
// 'nonceStr': '',
// 'package': '',
// 'signType': 'MD5',
// 'paySign': '',
// 'success':function(res){},
// 'fail':function(res){},
// 'complete':function(res){}
// })

