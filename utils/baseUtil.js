export default {
  //模型结构拷贝去引用
  modelCopy: function (data) {
    return JSON.parse(JSON.stringify(data))
  },
  
  isBlank:function (value) {
    if(value != undefined && value != null && value.toString().replace(/^\s+|\s+$/g, '') != '') {
      return false;
    }else return true;
  },
  isNotBlank:function (value) {
    return !this.isBlank(value);
  },
  isBlankObj:function (value) {
    if(value == null || value == undefined) {
      return true
    }else return false
  },
  isNotBlankObj:function (value) {
    return !this.isBlankObj(value);
  },
  isNotBlankObjs:function (...vals) {
    var bol  = false;
    if(vals == null || vals == undefined || vals.length == 0) return bol;
    vals.every(obj => {
      obj = obj == null || obj == undefined?'':obj+'';
      if(obj != undefined && obj != null &&  obj.toString().replace(/^\s+|\s+$/g, '') !== '') {
        bol = true;
        return false;
      }
    })
    return bol;
  },
  isUnDeleted:function (val) {
      if(val === 'UN_DELETED' || val == 0 || val == "0") return true;
      else return false;
  },
}
