//music.js
// export default class Music{
// 	constructor(){
// 		this.isLoading=false;
// 	}
// 	static playd(){
// 		const innerAudioContext = uni.createInnerAudioContext();
// 		innerAudioContext.autoplay = true;
// 		innerAudioContext.src = 'https://www.xxx.com/didi.mp3';
// 		innerAudioContext.onPlay(() => {
// 			console.log('开始播放');
// 		});
// 		innerAudioContext.onError((res) => {
// 			console.log('播放错误');
// 		  	console.log(res.errMsg);
// 		  	console.log(res.errCode);
// 		});
// 	}
// }
// Music.isLoading = false;
var music={
		// 如果有更多声音的在下面接着创建
		
		playd(type){
			const innerAudioContext = uni.createInnerAudioContext();
			innerAudioContext.autoplay = true;
			innerAudioContext.src = '../../static/images/'+type+'.mp3';
			innerAudioContext.onPlay(() => {
		  		console.log('开始播放');
			});
			innerAudioContext.onError((res) => {
				console.log('播放错误');
			  	console.log(res.errMsg);
			  	console.log(res.errCode);
			});
	},
}

module.exports = music;

