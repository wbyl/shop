import storageUtil from './storageUtil'
import store from '../store/index'

export function isLogin(){
	console.log('vtoken ==>', store.state.vuex_token)
	return store.state.vuex_token != '';
}
export function checkLogin(isNavigateToLogin = true){
	console.log("token:", store.state.vuex_token)
	if(!store.state.vuex_token){
		storageUtil.setItem('redirectUrl', getCurPageUrl());
		console.log('isNavigateToLogin:', isNavigateToLogin,"redirectUrl:",getCurPageUrl());
		if(isNavigateToLogin == true){
			uni.reLaunch({
				url:'/pages/public/oauth'
			});
		}
		return false;
	}else{
		return true;
	}
}
export function getCurPageUrl(){
	
	/*获取当前路由*/
	let curPage = getCurPage();
	//在微信小程序或是app中，通过curPage.options；如果是H5，则需要curPage.$route.query（H5中的curPage.options为undefined，所以刚好就不需要条件编译了）
	let options = curPage.options;
	console.log('options=>',options)
	let url = curPage.route;
	if(options != null){
		let query = ''
		for(let k in options){
			if(query != ''){
				query+='&';
			}
			query+= k+'='+ options[k];
		}
		if(query != ''){
			url+='?'+query
		}
	}
	console.log('url==>',url)
	if(!url.startsWith('/')){
		url = '/' + url;
	}
	return url;
}
export function getCurPage(){
    let pages = getCurrentPages();
    let curPage = pages[pages.length-1];
    return curPage
}
