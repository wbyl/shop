export default class NavigateUtil {
	static switchTabs = ['/pages/index/index','/pages/job/index','/pages/user/index'];
	static isTab(url) {
		return this.switchTabs.some(path => url.startsWith(path));
	}
	/**
	 * 如果能够后退（多层），则navigateBack，否则调用redirectTo
	 */
	static goto(url,isReLaunch = false) {
		if(isReLaunch){
			uni.reLaunch({
				url: url
			});
			return false;
		} 
		if (this.isTab(url)) {
			console.log('is=tab==>',true)
			uni.switchTab({
				url: url
			});
			return false;
		} 
		const pages = getCurrentPages()
		// route在低版本不兼容
		const index = pages.findIndex(item => ('/' + item.__route__) === url)
		if (pages.length < 2 || index < 0) {
			uni.redirectTo({
				url: url
			});
		} else {
			const delta = pages.length - 1 - index
			uni.navigateBack({
				delta: delta
			});
		}
	}
	
}
