import Vue from 'vue'
import App from './App'
import queryUtil from './utils/queryUtil'
import baseUtil from './utils/baseUtil'
import storageUtil from './utils/storageUtil'
import NavigateUtil from './utils/navigateUtil.js'
import uniRequest from './utils/request'
import store from '@/store';
import tip from'./utils/tip.js'

const web = "http://demo.tudouduanshiping.com";
const apiurl = web + "/public/index.php/api/";
const imgurl = web + "/public/uploads/";
const imgurl2 = "/public/uploads/";
App.mpType = 'app'
// main.js，注意要在use方法之后执行
import uView from 'uview-ui'
Vue.use(uView)
// 如此配置即可
uni.$u.config.unit = 'rpx'
Vue.config.productionTip = false


// i18n部分的配置
// 引入语言包，注意路径
import Chinese from '@/common/locales/zh.js';
import English from '@/common/locales/en.js';

// VueI18n
import VueI18n from '@/common/vue-i18n.min.js';

// VueI18n
Vue.use(VueI18n);

const msg = (title, duration=1500, mask=false, icon='none')=>{
	//统一提示方便全局修改
	if(Boolean(title) === false){
		return;
	}
	uni.showToast({
		title,
		duration,
		mask,
		icon
	});
}

const tips = (title, duration=1500, mask=false, icon='none')=>{
	//统一提示方便全局修改
	if(Boolean(title) === false){
		return;
	}
	uni.showToast({
		title,
		duration,
		mask,
		icon
	});
}


const json = type=>{
	//模拟异步请求数据
	return new Promise(resolve=>{
		setTimeout(()=>{
			resolve(Json[type]);
		}, 500)
	})
}

const toDown = (title)=>{
	uni.showModal({
		title:title,
		success(res) {
			console.log(res);
			if(res.confirm){
				console.log('成功');
				uni.navigateTo({
					url:'/pages/download/download'
				})
			}
		}
	})
}

const prePage = ()=>{
	let pages = getCurrentPages();
	let prePage = pages[pages.length - 2];
	// #ifdef H5
	return prePage;
	// #endif
	return prePage.$vm;
}


const req = (method, url, data) => {
	const promise = new Promise((resolve, reject) => {
		// promise里面都可以处理任何错误捕捉，包括请求前，请求后
		if (data && data.loading) {
			uni.showLoading({
				title: '加载中...',
				mask: true
			})
		}
		
		url = apiurl + url;
		
		uni.request({
			method,
			url,
			data,
			header: {
			 	//'Content-Type': 'application/json;charset=UTF-8',
			 	//Accept: 'application/json, text/plain, */*',
				token: uni.getStorageSync('token') || ''
			},
			success: (res) => {
				if(res.data.code === 401) {
					reject(res)
				} else {
					resolve(res)
				}
				if (data && data.loading) {
					uni.hideLoading()
				}
			},
			fail: (err) => {
				reject(err)
				let msg = err.errMsg
				if (msg.indexOf('timeout') > -1) {
					msg = '连接超时'
				}
				if (msg.indexOf('abort') > -1) {
					msg = '连接终止'
				}
				uni.showToast({ 
					title: msg || '网络错误！',
					icon: 'none'
				})
			}
		});
	});
	return promise
}

const ajax = {
	get: (url, data) => {
		return req('GET', url, data)
	},
	post: (url, data) => { 
		return req('POST', url, data)
	},
	put: (url, data) => {
		return req('PUT', url, data)
	},
	del: (url, data) => {
		return req('DELETE', url, data)
	},
}

Vue.prototype.$fire = new Vue();
Vue.prototype.$api = {msg, json, prePage, tips,toDown};


const i18n = new VueI18n({
	// 默认语言
	locale: 'zh',
	// 引入语言文件
	messages: {
		'zh': Chinese,
		'en': English,
	}
});


// 由于微信小程序的运行机制问题，需声明如下一行，H5和APP非必填
Vue.prototype._i18n = i18n;
Vue.config.productionTip = false;
Vue.prototype.baseUtil = baseUtil;
Vue.prototype.qUtil = queryUtil;
Vue.prototype.storageUtil = storageUtil;
Vue.prototype.navigationUtil = NavigateUtil;
Vue.prototype.uniRequest = uniRequest;
Vue.prototype.$store = store;
const app = new Vue({
   i18n,
   store,
   ...App
})
Vue.prototype.$ajax = ajax
Vue.prototype.web = web
Vue.prototype.apiurl = apiurl
Vue.prototype.imgurl = imgurl
app.$mount()
