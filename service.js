// 管理账号信息
const USERS_KEY = 'USERS_KEY';
const TOKEN_KEY = 'token';
const STATE_KEY = 'STATE_KEY';

const getUsers = function() {
	let ret = '';
	ret = uni.getStorageSync(USERS_KEY);
	if (!ret) {
		ret = '[]';
	}
	return JSON.parse(ret);
}

const getUser = function(url) {
	console.log(url);
	let ret = '';
	ret = uni.getStorageSync(USERS_KEY);
	if (!ret) {
		ret = '[]';
		return JSON.parse(ret);
	} else {
		ret = JSON.parse(ret);
		
		var url = url + "/users/check.html"
		
		uni.request({
		    url: url, 
		    data: {
		        token: ret.token,
		    },
		    // header: {
		    //     'custom-header': 'hello' //自定义请求头信息
		    // },
		    success: function (res) {
		        console.log(res);
				
				//return JSON.parse(ret);
		    }
		});	
	}
}

const refreshUser = function(url) {
	console.log(url);
	let ret = '';
	// ret = uni.getStorageSync(USERS_KEY);
	if (!ret) {
		ret = '[]';
		return JSON.parse(ret);
	} else {
		ret = JSON.parse(ret);
		
		var url = url + "/users/getUserInfo.html"
		
		uni.request({
		    url: url, 
		    data: {
		        token: ret.token,
		    },
		    // header: {
		    //     'custom-header': 'hello' //自定义请求头信息
		    // },
		    success: function (res) {
		        console.log(res);
				
				//return JSON.parse(ret);
		    }
		});	
	}
}

const addUser = function(userInfo) {
	let users = getUsers();
	
	//if()
	
	users.push({
		id: userInfo.id,
		name: userInfo.name,
		mobile: userInfo.mobile,
		smsCode: userInfo.smsCode,
		password: userInfo.password,
		rcode: userInfo.rcode,
		img: userInfo.img,
		//token: userInfo.token,
	});
	
	//uni.setStorageSync(USERS_KEY, "");
	uni.setStorageSync(TOKEN_KEY, userInfo.token);
	uni.setStorageSync(USERS_KEY, users);
}

const updateUser = function(userInfo) {
	var user = {
		id: userInfo.id,
		name: userInfo.name,
		mobile: userInfo.mobile,
		smsCode: userInfo.smsCode,
		password: userInfo.password,
		rcode: userInfo.rcode,
		img: userInfo.img,
	};
	
	uni.setStorageSync(TOKEN_KEY, userInfo.token);
	uni.setStorageSync(USERS_KEY, userInfo);
}

const delUser = function() {
	var user = {
		id: "",
		name: "",
		mobile: "",
		smsCode: "",
		password: "",
		rcode: "",
		img: "",
	};
	
	uni.setStorageSync(TOKEN_KEY, "");
	uni.setStorageSync(USERS_KEY, "");
	uni.clearStorageSync();
}

const logOut = function() {
	uni.setStorageSync(TOKEN_KEY, "");
	uni.setStorageSync(USERS_KEY, "");
}

const getToken = function() {
	return uni.getStorageSync(TOKEN_KEY) ? uni.getStorageSync(TOKEN_KEY) : '';
}

const isLogin = function() {
	var token = uni.getStorageSync(TOKEN_KEY);
	
	// console.log("token",token);
	// console.log(token !== "");
	
	if(token != undefined && token.length > 0) {
		return true;
		// var result = (await this.$ajax.get('users/getUserInfo.html'));
		// service.updateUser(result.data.data);
	} else {
		return false;
	}
}

const setToken = function(token) {
	uni.setStorageSync(TOKEN_KEY, token);
}

export default {
	getUsers,
	getUser,
	addUser,
	updateUser,
	delUser,
	logOut,
	getToken,
	setToken,
	isLogin,
}
