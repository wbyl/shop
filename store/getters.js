const getters = {
    // device: state => state.device,
    // hasLogin: state => state.hasLogin,
    token: state => state.vuex_token,
    userInfo: state => state.vuex_user
}
export default getters
