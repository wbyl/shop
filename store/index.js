import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
import uniRequest from '../utils/request.js'
import qUtil from '../utils/queryUtil.js'
import storageUtil from '../utils/storageUtil.js'
import getters from './getters.js'
let lifeData = {};

try{
	// 尝试获取本地是否存在lifeData变量，第一次启动APP时是不存在的
	lifeData = uni.getStorageSync('lifeData') || {};
}catch(e){
	
}

// 需要永久存储，且下次APP启动需要取出的，在state中的变量名
let saveStateKeys = ['vuex_user', 'vuex_token'];

// 保存变量到本地存储中
const saveLifeData = function(key, value){
	// 判断变量名是否在需要存储的数组中
	if(saveStateKeys.indexOf(key) != -1) {
		// 获取本地存储的lifeData对象，将变量添加到对象中
		let tmp = uni.getStorageSync('lifeData');
		// 第一次打开APP，不存在lifeData变量，故放一个{}空对象
		tmp = tmp ? tmp : {};
		tmp[key] = value;
		// 执行这一步后，所有需要存储的变量，都挂载在本地的lifeData对象中
		uni.setStorageSync('lifeData', tmp);
	}
}
const store = new Vuex.Store({
	state: {
		// 如果上面从本地获取的lifeData对象下有对应的属性，就赋值给state中对应的变量
		// 加上vuex_前缀，是防止变量名冲突，也让人一目了然
		vuex_user: lifeData.vuex_user ? lifeData.vuex_user : {name: '游客'},
		vuex_token: lifeData.vuex_token ? lifeData.vuex_token : '',
		// 如果vuex_version无需保存到本地永久存储，无需lifeData.vuex_version方式
		vuex_version: '1.0.1',
		vuex_demo: '绛紫',
		// 自定义tabbar数据
		vuex_tabbar: [{
				pagePath: '/pages/index/index',
				iconPath: "/static/images/tab_home.png",
				selectedIconPath: "/static/images/tab_home_current.png",
				text: '首页'
				
			},
			{
				pagePath: '/pages/club/club',
				iconPath: "/static/images/tab_club.png",
				selectedIconPath: "/static/images/tab_club_current.png",
				text: '互动交流'
			},
			{
				pagePath: '/pages/job/index',
				iconPath: "/static/images/tab_job.png",
				selectedIconPath: "/static/images/tab_job_current.png",
				text: '热招职位',
				midButton: true
			},
			{
				pagePath: '/pages/live/index',
				iconPath: "/static/images/tab_live.png",
				selectedIconPath: "/static/images/tab_live_current.png",
				text: '空宣直播'
			},
			{
				pagePath: '/pages/user/index',
				iconPath: "/static/images/tab_my.png",
				selectedIconPath: "/static/images/tab_my_current.png",
				text: '我的'
			}
		]
	},
	mutations: {
		SET_TOKEN(state,payload){
			state.vuex_token = payload;
			lifeData.vuex_token = payload;
			saveLifeData(saveStateKeys[1], payload);
		},
		SET_USER(state,payload){
			state.vuex_user = payload;
			lifeData.vuex_user = payload;
			saveLifeData(saveStateKeys[0], payload);
		},
		$uStore(state, payload) {
			// 判断是否多层级调用，state中为对象存在的情况，诸如user.info.score = 1
			let nameArr = payload.name.split('.');
			let saveKey = '';
			let len = nameArr.length;
			if(len >= 2) {
				let obj = state[nameArr[0]];
				for(let i = 1; i < len - 1; i ++) {
					obj = obj[nameArr[i]];
				}
				obj[nameArr[len - 1]] = payload.value;
				saveKey = nameArr[0];
			} else {
				// 单层级变量，在state就是一个普通变量的情况
				state[payload.name] = payload.value;
				saveKey = payload.name;
			}
			// 保存变量到本地，见顶部函数定义
			saveLifeData(saveKey, state[saveKey])
		}
	},
	actions: {
		login({commit}, payload) {
			console.log('loginto==>',payload)
			return new Promise(async (resolve,reject)=>{
				let resp = await uniRequest.post('/oauth', payload);
				console.log("resp=====>",resp)
				if(!qUtil.validResp(resp)){
					reject(resp.msg || '授权登录失败');
				}else{
					commit('SET_TOKEN', resp.data.token);
					commit('SET_USER', resp.data);
					resolve(resp.data);
				}
			});
		},
		logout({commit}){
			commit('SET_TOKEN', '');
			commit('SET_USER', {});
			storageUtil.removeItem('currentAddress');
		},
		refreshUserInfo({commit}, payload){
			commit('SET_USER', payload);
		},
		refreshInfo({commit}){
			return new Promise(async (resolve,reject)=>{
				let resp = await uniRequest.get('/oauth/userInfo');
				console.log("userInfo.resp=====>",resp)
				if(!qUtil.validResp(resp)){
					reject(resp.msg || '登录信息过期');
				}else{
					// commit('SET_TOKEN', resp.data.token);
					commit('SET_USER', resp.data);
					resolve(resp.data);
				}
			});
		}
	},
	getters
})

export default store
